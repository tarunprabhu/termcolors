#!/usr/bin/env sh
#
# -----------------------------------------------------------------------------
#
#  Copyright (c) 2022, Tarun Prabhu
#
#  This program is free software: you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by the
#  Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see <https://www.gnu.org/licenses/>.
#
# -----------------------------------------------------------------------------
#
#  Print a color table similar to the one produced by xfce4-terminal with
#  the --colortable flag.
#
#  USAGE
#
#      termcolors [OPTIONS]
#
#  OPTIONS
#
#      -h, --help       Print this help message.
#      --normal         Show normal text.
#      --bold           Show bold text.
#      --dim            Show dim text.
#      --no-normal      Do not show normal text.
#      --no-bold        Do not show bold text.
#      --no-dim         Do not show dim text.
#      --bright         Use bright colors.
#
# -----------------------------------------------------------------------------

show_usage() {
    echo "Usage: termcolors [OPTIONS]"
    echo ""
    echo "Print a color table."
    echo ""
    echo "Options:"
    echo "   -h, --help       Print this help message."
    echo "   --normal         Show normal text."
    echo "   --bold           Show bold text."
    echo "   --dim            Show dim text."
    echo "   --no-normal      Do not show normal text."
    echo "   --no-bold        Do not show bold text."
    echo "   --no-dim         Do not show dim text."
    echo "   --bright         Use bright colors."
    echo ""
}

main() {
    local normal=1
    local bold=1
    local dim=0
    local bright=0

    while [ $# -ne 0 ]; do
        local arg=$1
        case "${arg}" in
            --normal)    normal=1 ;;
            --bold)      bold=1   ;;
            --dim)       dim=1    ;;
            --no-normal) normal=0 ;;
            --no-bold)   bold=0   ;;
            --no-dim)    dim=0    ;;
            --bright)    bright=1 ;;
            -h | --help)
                show_usage
                exit 0
                ;;
            -*)
                echo "error: unknown option ${arg}" >&2
                exit 1
                ;;
            *)
                echo "error: unknown argument ${arg}" >&2
                exit 1
                ;;
        esac
        shift
    done

    local base=30
    if [ ${bright} -eq 1 ]; then
        base=90
    fi

    local styles=""
    [ ${normal} -ne 0 ] && styles="${styles} 0"
    [ ${bold} -ne 0 ]   && styles="${styles} 1"
    [ ${dim} -ne 0 ]    && styles="${styles} 2"

    printf "%6s |%7s" " " " "
    for b in 0 1 2 3 4 5 6 7; do
        local bgc=$((${base} + ${b} + 10))
        printf "%6s " "${bgc}m"
    done
    printf "\n"

    for s in ${styles}; do
        printf "%6s |\033[${s}m%6s \033[0m" "${s}m" "${s}m"
        for b in 0 1 2 3 4 5 6 7; do
            local bg=$((${base} + ${b} + 10))
            printf "\033[0;${s};${bg}m%6s \033[0m" "${s}m"
        done
        printf "\n"
    done

    for s in ${styles}; do
        for f in 0 1 2 3 4 5 6 7; do
            local fg=$((${base} + ${f}))
            local text=""
            case "${s}" in
                0) text="${fg}"    ;;
                1) text="1;${fg}" ;;
                2) text="2;${fg}" ;;
            esac
            printf "%6s |\033[${s};${fg}m%6s \033[0m" "${text}m" "${text}m"
            for b in 0 1 2 3 4 5 6 7; do
                local bg=$((${base} + ${b} + 10))
                printf "\033[${s};${fg};${bg}m%7s\033[0m" "${text}m "
            done
            printf "\n"
        done
    done
}

main $@
