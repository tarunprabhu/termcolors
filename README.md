# termcolors

`termcolors` prints a color table to the terminal similar to the one produced
by XFCE terminal when called with the `--color-table` flag. `termcolors` has 
options to the terminals' rendering of additional ANSI styles such as dim and 
reverse. 

## Requiremnts

`termcolors` has no required dependencies and should work on any system with a
POSIX shell. 

## Installation

The `termcolors` script can be copied to any location where it is reachable 
from `$PATH`. 

## Usage

Run `termcolors` on the terminal prompt. For additional options, use 
`termcolors -h`. 

## TODO

- Support more video modes such as italic, underline and reverse. 
- Add support to print information about the terminal and color support.
- Add options to print different table styles.
- Add option to print custom text instead of the ANSI code.
